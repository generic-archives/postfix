# Dockerfile for Postfix container and MailParser service

Environment variables:

| Name | Value | Comment |
|------|-------|---------|
| GENERICARCHIVES_MXDNS | zzzarchives.cern.ch | mandatory, the MX domain where the mails will be send |
| GENERICARCHIVES_DOMAIN | cern.ch | optional. Domain name for the Postfix server. Will use MX domain if not set |
| GENERICARCHIVES_HOSTNAME | servername | optional. Will use the self generated if not set. |
| GENERICARCHIVES_VHOSTBASEFOLDER | /var/mail/vhosts (default) | optional. Avoid due to dockerfile chmod, prefer a symlink inside for each vhost subfolder |
| GENERICARCHIVES_VIRTUALMAILBOXMAPS | /etc/postfix/vmailbox | optional. Avoid due to dockerfile chmod, prefer a symlink inside for each vhost subfolder |
| GENERICARCHIVES_ENABLE_TLS | true | optional. True will enable postfix over TLS with generated certs |

- GENERICARCHIVES_VHOSTBASEFOLDER defaults to ```/var/mail/vhosts```.
Should be hosted on a persistent storage, or eventually on EOS directly (to be tested depending on traffic)
- GENERICARCHIVES_VIRTUALMAILBOXMAPS defaults to ```/etc/postfix/vmailbox```. Could be hosted also on persistent, otherwise will need to be rebuilt at each redeploy. To be tested: symlink the vmailbox to EOS, keep local the db.

### GENERICARCHIVES_ENABLE_TLS true 
(inspired from https://github.com/container-images/postfix)
requires host certificates
```
cd certs
openssl req -new -newkey rsa:2048 -nodes -keyout localhost.key -out localhost.csr -subj "/C=CH/ST=Geneva/L=Geneva/O=CERN/OU=IT-CDA/CN=localhost"
openssl x509 -req -days 365 -in localhost.csr -signkey localhost.key -out localhost.crt
```
Testing
```
openssl s_client -starttls smtp -crlf -connect localhost:25
```


### Deploy to OpenShift
oc login https://openshift.cern.ch
oc project whatever

oc new-app https://gitlab.cern.ch/generic-archives/postfix.git

oc status
