FROM centos:centos7
LABEL maintainer="emmanuel.ormancey@cern.ch"

# install necessary packages
#RUN rpm --import -vv /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-* && yum install -y epel-release && yum update -y && yum -y install postfix && yum clean all
#RUN yum update --nogpgcheck -y && yum -y --nogpgcheck install postfix && yum clean all

# Supervisor 
#RUN sed -i -e "s/^nodaemon=false/nodaemon=true/" /etc/supervisord.conf

# Postfix 3 on CernOS7 addiitonal repository
COPY gf.repo /etc/yum.repos.d/gf.repo

RUN yum clean all && yum makecache fast && yum --nogpgcheck -y install postfix3 telnet

# copy docker entrypoint script
ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

# Copy certificates if any in case TLS is enabled
RUN mkdir -p /etc/postfix/certs
COPY certs/* /etc/postfix/certs/

# Prepare virtual hosts user and group
RUN groupadd -g 5000 genericarchives && useradd -M -u 5000 -g 5000 genericarchives && usermod -L genericarchives
# Set full perm on folders
RUN mkdir -p /var/mail/vhosts && chmod -R a+rwx /var/mail/vhosts && chmod -R a+rwx /etc/postfix

# during container start, ensure proper directory rights and start Postfix
ENTRYPOINT ["/bin/sh","-c","/usr/local/bin/docker-entrypoint.sh"]

EXPOSE 25 587 2525 10025
