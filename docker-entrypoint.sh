#!/bin/bash

[ "${DEBUG}" == "yes" ] && set -x

# Set Postfix Config value function
function add_config_value() {
  local key=${1}
  local value=${2}
  local config_file=${3:-/etc/postfix/main.cf}
  [ "${key}" == "" ] && echo "ERROR: No key set !!" && exit 1
  [ "${value}" == "" ] && echo "ERROR: No value set !!" && exit 1

  echo "Setting configuration option ${key} with value: ${value}"
  postconf -e "${key} = ${value}"
}

if [ -z "${GENERICARCHIVES_MXDNS}" ]
then
    echo "Missing environment variable GENERICARCHIVES_MXDNS"
    exit -1
fi

# Setup conf from environment if present
vhostbasefolder="${GENERICARCHIVES_VHOSTBASEFOLDER:-/var/mail/vhosts}"
virtualmailboxmaps="${GENERICARCHIVES_VIRTUALMAILBOXMAPS:-/etc/postfix/vmailbox}"
domain="${GENERICARCHIVES_DOMAIN:-$GENERICARCHIVES_MXDNS}"

# Create user and group uid 5000 gid 5000
# Moved to dockerfile as root
#groupadd -g 5000 genericarchives
#useradd -M -u 5000 -g 5000 genericarchives
#usermod -L genericarchives

# Create folder for MXDNS (e.g. /var/mail/vhosts/groupsarchives.cern.ch)
vhostfolder=${vhostbasefolder}/${GENERICARCHIVES_MXDNS}
mkdir -p $vhostfolder
# chmod done in dockerfile as root
#chgrp -R 5000 $vhostfolder
#chmod g+rwx $vhostfolder

# Main postfix configuration
add_config_value "inet_interfaces" "all"
if [ ! -z "${GENERICARCHIVES_HOSTNAME}" ]
then
    hostname=`echo "${GENERICARCHIVES_HOSTNAME}.${domain}"`
    add_config_value "myhostname" ${hostname}
fi
add_config_value "mydomain" ${domain}
add_config_value "mydestination" "\$myhostname, localhost.\$mydomain, localhost"
add_config_value "local_recipient_maps" "\$virtual_mailbox_maps"
# disable outgoing mails
add_config_value "default_transport" "error: This server sends mail only locally."
# Log to stdout
add_config_value "maillog_file" "/dev/stdout"

# virtual domain handling
# http://www.postfix.org/VIRTUAL_README.html
add_config_value "virtual_mailbox_domains" ${GENERICARCHIVES_MXDNS}
add_config_value "virtual_mailbox_base" ${vhostbasefolder}
add_config_value "virtual_mailbox_maps" "hash:${virtualmailboxmaps}"
add_config_value "virtual_minimum_uid" "100"
add_config_value "virtual_uid_maps" "static:5000"
add_config_value "virtual_gid_maps" "static:5000"
add_config_value "virtual_alias_maps" "hash:/etc/postfix/virtual"

# Create vmailbox file is missing
if [ ! -f "$virtualmailboxmaps" ]; then
    echo "$virtualmailboxmaps does not exists, creating empty file."
    touch $virtualmailboxmaps
    echo "# Comment out the entry below to implement a catch-all. Must be the last line." >> $virtualmailboxmaps
    echo "#@$GENERICARCHIVES_MXDNS        $GENERICARCHIVES_MXDNS/catchall" >> $virtualmailboxmaps
fi

# Build dbs
postmap /etc/postfix/virtual
postmap ${virtualmailboxmaps}

# Subnet restricitons to allow mails only from CERN mail servers
#Check for subnet restrictions
# nets='10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16'
# if [ ! -z "${SMTP_NETWORKS}" ]; then
#         for i in $(sed 's/,/\ /g' <<<$SMTP_NETWORKS); do
#                 if grep -Eq "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/[0-9]{1,2}" <<<$i ; then
#                         nets+=", $i"
#                 else
#                         echo "$i is not in proper IPv4 subnet format. Ignoring."
#                 fi
#         done
# fi
# add_config_value "mynetworks" "${nets}"

# Enable TLS if specified
if [ ! -z "${GENERICARCHIVES_ENABLE_TLS}" ]
then
    echo "Enabling TLS..."
    if [[ -n "$(find /etc/postfix/certs -iname *.crt)" && -n "$(find /etc/postfix/certs -iname *.key)" ]]; then
        echo "Changing postfix configuration file for using TLS."
        add_config_value "smtpd_use_tls" "yes"
        add_config_value "smtpd_tls_auth_only" "no"
        add_config_value "smtpd_tls_CAfile" $(find /etc/postfix/certs -iname cacert.pem)
        add_config_value "smtpd_tls_key_file" $(find /etc/postfix/certs -iname *.key)
        add_config_value "smtpd_tls_cert_file" $(find /etc/postfix/certs -iname *.crt)
        add_config_value "smtpd_tls_loglevel" "3"
        add_config_value "smtpd_tls_received_header" "yes"
        add_config_value "smtpd_tls_session_cache_timeout" "3600s"
        add_config_value "smtpd_tls_security_level" "may"
        #add_config_value "tls_random_source" "dev:/dev/urandom"
    else
        echo "Error configuring TLS, missing certificates in /etc/postfix/certs."
    fi
fi

# Check config and create missing directories
# Disabled as start-fg does it as well
#postfix check

# Remove stuff
# Remove this, as it triggers warnings but it's empty
rm -f /etc/postfix/dynamicmaps.cf

# Change default port to 2525
sed -i 's/^smtp.*inet/2525\tinet/g' /etc/postfix/master.cf

# =====================================
# Start services

# If host mounting /var/spool/postfix, we need to delete old pid file before
# starting services
rm -f /var/spool/postfix/pid/master.pid

echo "Starting postfix start-fg"
exec postfix start-fg